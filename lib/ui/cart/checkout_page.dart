import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_login_screen/model/product.dart';
import 'package:flutter_login_screen/services/authenticate.dart';
import 'package:intl/intl.dart';

import '../../constants.dart';

class CheckOutPage extends StatefulWidget {
  final List<Product> cart;

  CheckOutPage(this.cart);

  _CheckOutPageState createState() => _CheckOutPageState();
}

class _CheckOutPageState extends State<CheckOutPage>
    with SingleTickerProviderStateMixin {
  var now = DateTime.now();

  get weekDay => DateFormat('EEEE').format(now);

  get day => DateFormat('dd').format(now);

  get month => DateFormat('MMMM').format(now);
  double oldTotal = 0;
  double total = 0;
  double oldSubtotal = 0;
  double subtotal = 0;
  double oldServiceFee = 0;
  double serviceFee = 0;
  double totalWithServiceFee = 0;

  ScrollController scrollController = ScrollController();
  AnimationController animationController;

  onCheckOutClick() async {}

  @override
  void initState() {
    animationController =
        AnimationController(duration: Duration(milliseconds: 200), vsync: this)
          ..forward();

    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('CheckOut'), centerTitle: true,backgroundColor: Color(COLOR_GREEN),),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ...buildHeader(),
                //cart items list
                widget.cart.length != 0
                    ? ListView.builder(
                        itemCount: widget.cart.length,
                        shrinkWrap: true,
                        controller: scrollController,
                        itemBuilder: (BuildContext context, int index) {
                          return buildCartItemList(widget.cart[index]);
                        },
                      )
                    : Center(
                        child: Text("Please add items in cart to continue."),
                      ),
                SizedBox(height: 16),
                Text(
                  "Cart Total",
                  style: TextStyle(
                      color: Colors.green,
                      fontSize: 22,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.left,
                ),

                Divider(),
                buildPriceInfo(widget.cart),
                //checkoutButton(context),
              ],
            ),
          ),
        ));
  }

  List<Widget> buildHeader() {
    return [
      Padding(
        padding: const EdgeInsets.only(top: 24.0, bottom: 32),
        child: Text('$weekDay, ${day}th of $month ',
            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
      ),
    ];
  }

  void removeFromCart(Product product) async {
    FireStoreUtils.updateProduct(product
      ..addedToCart = false
      ..qty = 1);
  }

  void updateCart(Product product) async {
    FireStoreUtils.updateProduct(product);
  }

  Widget buildPriceInfo(List<Product> cart) {
    oldTotal = total;
    total = 0;
    oldSubtotal = subtotal;
    subtotal = 0;
    oldServiceFee = serviceFee;
    serviceFee = 0;
    for (Product cartModel in cart) {
      total += (cartModel.price) * cartModel.qty;
      subtotal = total;
      serviceFee = total > 39 ? 0.00 : 5.00;
    }

    //oldTotal = total;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   children: <Widget>[
        //     Text('Subtotal',
        //         style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
        //     AnimatedBuilder(
        //       animation: animationController,
        //       builder: (context, child) {
        //         return Text(
        //             '\$ ${lerpDouble(oldSubtotal, subtotal, animationController.value).toStringAsFixed(2)}',
        //             style:
        //                 TextStyle(fontSize: 14, fontWeight: FontWeight.w500));
        //       },
        //     ),
        //   ],
        // ),
        SizedBox(
          height: 4,
        ),
        /*Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('GST', style: subtitleStyle),
            AnimatedBuilder(
              animation: animationController,
              builder: (context, child) {
                return Text(
                    '\$ ${lerpDouble(oldgst, gst, animationController.value).toStringAsFixed(2)}',
                    style: subtitleStyle);
              },
            ),
          ],
        ),
        SizedBox(
          height: 4,
        ),*/
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   children: <Widget>[
        //     Text('Service Fee',
        //         style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
        //     AnimatedBuilder(
        //       animation: animationController,
        //       builder: (context, child) {
        //         return Text(
        //             '\$ ${lerpDouble(oldServiceFee, serviceFee, animationController.value).toStringAsFixed(2)}',
        //             style:
        //                 TextStyle(fontSize: 14, fontWeight: FontWeight.w500));
        //       },
        //     ),
        //   ],
        // ),
        SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Total',
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
            AnimatedBuilder(
              animation: animationController,
              builder: (context, child) {
                return Text(
                    '\₹ ${lerpDouble(oldTotal, total , animationController.value).toStringAsFixed(2)}',
                    style:
                        TextStyle(fontSize: 22, fontWeight: FontWeight.bold));
              },
            ),
          ],
        ),
        SizedBox(
          height: 16,
        ),
        // (total) < 39.00
        //     ? Align(
        //         alignment: Alignment.center,
        //         child: Text(
        //           "Please add more product of \$ ${(39.00 - (total)).toStringAsFixed(2)} for free service.",
        //           textAlign: TextAlign.center,
        //           style: TextStyle(color: Colors.green, fontSize: 16),
        //         ),
        //       )
        //     : SizedBox()
      ],
    );
  }

  Widget checkoutButton(context) {
    return Container(
      margin: EdgeInsets.only(top: 24, bottom: 64),
      width: double.infinity,
      child: RaisedButton(
        child: Text('Checkout',
            style: TextStyle(fontSize: 16, color: Colors.white)),
        onPressed: () {
          onCheckOutClick();
        },
        padding: EdgeInsets.symmetric(horizontal: 64, vertical: 12),
        color: Color(COLOR_GREEN),
        shape: StadiumBorder(),
      ),
    );
  }

  Widget buildCartItemList(Product cartModel) {
    return Card(
      margin: EdgeInsets.only(bottom: 16),
      child: Container(
        height: 100,
        padding: EdgeInsets.all(8),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // CircleAvatar(
            //     backgroundImage: AssetImage('assets/images/groceries.png')),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  height: 45,
                  child: Text(
                    cartModel.name,
                    style:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.start,
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    InkWell(
                      customBorder: RoundedRectangleBorder(
                        borderRadius: BorderRadiusDirectional.circular(4),
                      ),
                      onTap: () {
                        /*   cart.decreaseItem(cartModel);*/
                        setState(() {
                          if (cartModel.qty != 1) {
                            cartModel.qty = (cartModel.qty - 1);
                            updateCart(cartModel);
                          }
                        });

                        animationController.reset();
                        animationController.forward();
                      },
                      child: Icon(Icons.remove_circle),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.0, vertical: 2),
                      child: Text('${cartModel.qty}',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                    ),
                    InkWell(
                      customBorder: RoundedRectangleBorder(
                        borderRadius: BorderRadiusDirectional.circular(4),
                      ),
                      onTap: () {
                        /*print("icrease ${cartModel.food.price}");
                        cart.increaseItem(cartModel);*/
                        setState(() {
                          cartModel.qty = (cartModel.qty + 1);
                        });
                        updateCart(cartModel);
                        animationController.reset();
                        animationController.forward();
                      },
                      child: Icon(Icons.add_circle),
                    ),
                  ],
                )
              ],
            ),
            Flexible(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: [
                      Container(
                        width: 70,
                        child: Text(
                          "\₹ ${cartModel.price}",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.end,
                        ),
                      ),
                    ],
                  ),
                  InkWell(
                    onTap: () {
                      removeFromCart(cartModel);
                      setState(() {
                        widget.cart.remove(cartModel);
                      });
                      if (widget.cart.isEmpty) {
                        /*  Fluttertoast.showToast(
                            msg: "Please add products to checkout.",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            timeInSecForIos: 1,
                            textColor: Colors.white,
                            fontSize: 14.0);*/
                        Navigator.pop(context);
                      }
                      animationController.reset();
                      animationController.forward();
                    },
                    customBorder: RoundedRectangleBorder(
                      borderRadius: BorderRadiusDirectional.circular(12),
                    ),
                    child: Icon(Icons.delete_sweep, color: Colors.red),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
