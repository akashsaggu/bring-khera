import 'package:cached_network_image/cached_network_image.dart';

import 'package:flutter/material.dart';
import 'package:flutter_login_screen/constants.dart';
import 'package:flutter_login_screen/model/product.dart';
import 'package:flutter_login_screen/services/authenticate.dart';

import 'checkout_page.dart';


class CartBottomSheet extends StatefulWidget {
  @override
  _CartBottomSheetState createState() => _CartBottomSheetState();
}

class _CartBottomSheetState extends State<CartBottomSheet> {
  List<Product> cartItems;
  @override
  void initState() {
    getCartItems();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return cartItems != null && cartItems.isNotEmpty
        ? Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  child: Container(
                    width: 90,
                    height: 8,
                    decoration: ShapeDecoration(
                        shape: StadiumBorder(), color: Colors.black26),
                  ),
                ),
                buildTitle(),
                Divider(),
                buildItemsList(cartItems),
                Divider(),
                buildPriceInfo(cartItems),
                SizedBox(height: 8),
                addToCardButton(cartItems, context),
              ],
            ),
          )
        : noItemWidget();
  }

  Widget buildTitle(/*cart*/) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('Your Order', style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
        /*RaisedButton.icon(
          icon: Icon(Icons.delete_forever),
          color: Colors.red,
          shape: StadiumBorder(),
          splashColor: Colors.white60,
          onPressed: () {
            setState(() {
              //cart.clearCart();
            });
          },
          textColor: Colors.white,
          label: Text('Clear'),
        ),*/
      ],
    );
  }

  Widget buildItemsList(List<Product> cartItems) {
    return Expanded(
      child: ListView.builder(
        itemCount: cartItems.length,
        physics: BouncingScrollPhysics(),
        itemBuilder: (context, index) {
          return Card(
            child: ListTile(
              // leading: CircleAvatar(
              //     backgroundImage: AssetImage(
              //         'assets/images/groceries.png')),
              title: Text('${cartItems[index].name}', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
              subtitle: Row(
                children: [
                  Text(
                      "\₹ ${cartItems[index].price}"),
                ],
              ),
              trailing: Text('x ${cartItems[index].qty}', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
            ),
          );
        },
      ),
    );
  }

  Widget noItemWidget() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('You don\'t have any product yet!!', style: TextStyle(fontSize: 16, color: Colors.black45)),
                  SizedBox(height: 16),
                  Icon(Icons.remove_shopping_cart, size: 40),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildPriceInfo(List<Product> cart) {
    double total = 0;
    for (Product cartModel in cart) {
      total +=
          (cartModel.price) *
              cartModel.qty;
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('Total:', style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
        Text('\₹ ${total.toStringAsFixed(2)}', style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
      ],
    );
  }

  Widget addToCardButton(List<Product> cartItems, context) {
    return Center(
      child: RaisedButton(
        child: Text('CheckOut',
            style: TextStyle(fontSize: 16, color: Colors.white)),
        onPressed: cartItems.isEmpty
            ? null
            : () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CheckOutPage(cartItems)));
              },
        padding: EdgeInsets.symmetric(horizontal: 64, vertical: 12),
        color: Color(COLOR_GREEN),
        shape: StadiumBorder(),
      ),
    );
  }

  void getCartItems() async {


    FireStoreUtils.getProductsInCart().then((value) {
      setState(() {
        cartItems = value;
      });
    });
  }
}
