import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login_screen/constants.dart';
import 'package:flutter_login_screen/main.dart';
import 'package:flutter_login_screen/model/product.dart';
import 'package:flutter_login_screen/model/user.dart';
import 'package:flutter_login_screen/services/authenticate.dart';
import 'package:flutter_login_screen/services/helper.dart';
import 'package:flutter_login_screen/ui/addItems/AddItemScreen.dart';
import 'package:flutter_login_screen/ui/auth/authScreen.dart';
import 'package:flutter_login_screen/ui/cart/cart_bottom_sheet.dart';
import 'package:flutter_login_screen/ui/updateItems/UpdateItemCart.dart';

class HomeScreen extends StatefulWidget {
  final User user;

  HomeScreen({Key key, @required this.user}) : super(key: key);

  @override
  State createState() {
    return _HomeState(user);
  }
}

class _HomeState extends State<HomeScreen> {
  final User user;

  _HomeState(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FloatingActionButton(
              heroTag: null,
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AddItemScreen()));
              },
              child: Icon(Icons.add),
              backgroundColor: Color(COLOR_GREEN),
            ),
            SizedBox(height: 10,),
            FloatingActionButton(
              heroTag: null,
              onPressed: () {
                FireStoreUtils.clearCart();

              },
              child: Icon(Icons.remove_shopping_cart),
              backgroundColor: Colors.redAccent,
            )
          ],
        ),
        appBar: AppBar(
          title: Text(
            'Bring Khera',
            style: TextStyle(color: Colors.black),
          ),
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          centerTitle: true,
          actions: [
            IconButton(icon: Icon(Icons.shopping_cart), onPressed: () {
              showModalBottomSheet(
                shape: RoundedRectangleBorder(
                    borderRadius:
                    BorderRadius.vertical(top: Radius.circular(40))),
                context: context,
                builder: (context) => CartBottomSheet(),
              );
            })
            ,IconButton(icon: Icon(Icons.logout), onPressed: () async {
            user.active = false;
            user.lastOnlineTimestamp = Timestamp.now();
            FireStoreUtils.updateCurrentUser(user);
            await auth.FirebaseAuth.instance.signOut();
            MyAppState.currentUser = null;
            pushAndRemoveUntil(context, AuthScreen(), false);
          })],
        ),
        body: ProductList());
  }

  String generateRandomString(int len) {
    var r = Random();
    return String.fromCharCodes(
        List.generate(len, (index) => r.nextInt(33) + 89));
  }
}

class ProductList extends StatefulWidget {
  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: FireStoreUtils.getProducts(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            final List<Product> productList = [];
            final queryDocSnapshot = snapshot.data.docs;
            queryDocSnapshot.forEach((element) {
              productList.add(Product.fromJson(element.data()));
            });
            if (productList.length > 0) {
              return ListView.builder(
                  itemCount: productList.length,
                  itemBuilder: (context, pos) {
                    return getProductUi(productList, pos);
                  });
            } else {
              return Center(
                  child: Text("No Products, Please add some products."));
            }
          } else if (snapshot.hasError) {
            return Center(child: Text("Sorry, something went wrong"));
          } else {
            return Center(child: CircularProgressIndicator());
          }
          return Text("");
        });
  }

  Widget getProductUi(List<Product> productList, int pos) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: ListTile(
          // leading: CircleAvatar(
          //     backgroundImage: AssetImage('assets/images/groceries.png')),
          title: Padding(
            padding: const EdgeInsets.only(top:8.0),
            child: Text('${productList[pos].name}',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
          ),
          subtitle: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text("\₹ ${productList[pos].price}"),
              MaterialButton(
                height: 30,
                child: Text(
                    '${productList[pos].addedToCart ? "Remove from cart" : "Add to cart"}',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w500)),
                color: Color(COLOR_GREEN),
                disabledColor: Color(COLOR_GREEN),
                onPressed: () async {
                  final product = productList[pos];
                  product.addedToCart = !product.addedToCart;
                  FireStoreUtils.updateProduct(product);
                },
              ),
            ],
          ),

          trailing: PopupMenuButton(
            icon: Icon(Icons.more_vert),
            itemBuilder: (context){
              return[
                PopupMenuItem(
                  value: 'edit',
                  child: Text('Edit'),
                ),
                PopupMenuItem(
                  value: 'delete',
                  child: Text('Delete'),
                ),
              ];
            },
            onSelected: (String value) => actionPopUpItemSelected(value, productList[pos]),
          )

        ),
      ),
    );
  }

  void actionPopUpItemSelected(String value, Product product) {


    if (value == 'edit') {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => UpdateItemScreen(product)));
    } else if (value == 'delete') {
     showDeleteDialog(context, product);
    } else {

    }

  }


  showEditProductDialog(BuildContext context,Product product){

  }


  showDeleteDialog(BuildContext context,Product product) {
    // set up the buttons
    Widget yes = FlatButton(
      child: Text("Yes"),
      onPressed:  () {
        FireStoreUtils.deleteProduct(product);
        Navigator.of(context).pop();
      },
    );
    Widget cancelButton = FlatButton(
      child: Text("No"),
      onPressed:  () {
        Navigator.of(context).pop();
      },
    );


    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Delete Product"),
      content: Text("Are you sure you want to delete the ${product.name}"),
      actions: [
        yes,
        cancelButton
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

}
