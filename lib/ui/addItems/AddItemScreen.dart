
import 'package:flutter/material.dart';
import 'package:flutter_login_screen/constants.dart';
import 'package:flutter_login_screen/model/product.dart';
import 'package:flutter_login_screen/services/authenticate.dart';

class AddItemScreen extends StatefulWidget{
  @override
  State createState() {
    return _AddItemScreenState();
  }

}

class _AddItemScreenState extends State<AddItemScreen>{

  TextEditingController _productNameController=TextEditingController();
  TextEditingController _productPriceController=TextEditingController();
  @override
  Widget build(BuildContext context) {
   return Scaffold(
     appBar: AppBar(
       title: Text("Add Product",style: TextStyle(color: Colors.black),),
       iconTheme: IconThemeData(color: Colors.black),
       backgroundColor: Colors.white,
       leading: IconButton(
         icon: Icon(Icons.arrow_back, color: Colors.black),
         onPressed: () => Navigator.of(context).pop(),
       ),
       centerTitle: true,
     ),
     body: SingleChildScrollView(
       child: Container(
child: Column(
  children: [
    Padding(
      padding: const EdgeInsets.only(left:16.0,right: 16,top:42,bottom: 24),
      child: TextField(
 controller: _productNameController,

        decoration: InputDecoration(
          border: OutlineInputBorder(),
          contentPadding: EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
          labelText: 'Enter Product Name',
        ),

      ),
    ),

    Padding(
      padding: const EdgeInsets.only(left:16.0,right: 16,bottom: 16),
      child: TextField(
        controller: _productPriceController,
keyboardType: TextInputType.number,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          contentPadding: EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
          labelText: 'Enter Product Price',
        ),

      ),
    ),

    Padding(
        padding: const EdgeInsets.only(left:16.0,right: 16,top: 16),
    child:SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 45,
      child: ElevatedButton(

        onPressed: (){
          if(validate()){
            var product= Product(name: _productNameController.text,price: double.parse(_productPriceController.text.toString()),qty: 1,addedToCart:false);
 FireStoreUtils.createProduct(product);
 Navigator.of(context).pop();

          }

        },
          child: Text("Add Product"),
        style: ElevatedButton.styleFrom(
          primary: Color(COLOR_PRIMARY),

          onPrimary: Colors.white,
          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
        ),

      ),
    )
    )
  ],
),
       ),
     ),
   );
  }


   bool validate(){
    if(
    _productPriceController.text!=null && _productNameController.text!=null
    ){
      return true;
    }else{
      return false;
    }
  }

}
