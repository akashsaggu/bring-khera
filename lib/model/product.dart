/// name : "Robot"
/// price : 500.3
/// qty : 3
/// added_to_cart : true

class Product {
  String id;
  String name;
  double price;
  int qty;
  bool addedToCart;
  int createdTimestamp;

  Product(
      {String id,
      String name,
      double price,
      int qty,
      bool addedToCart,
      int createdTimestamp}) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.qty = qty;
    this.addedToCart = addedToCart;
    this.createdTimestamp = DateTime.now().millisecondsSinceEpoch;
  }

  Product.fromJson(dynamic json) {
    this.id = json["id"];
    this.name = json["name"];
    this.price = json["price"];
    this.qty = json["qty"];
    this.addedToCart = json["added_to_cart"];
    this.createdTimestamp = json["createdTimestamp"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = this.id;
    map["name"] = this.name;
    map["price"] = this.price;
    map["qty"] = this.qty;
    map["added_to_cart"] = this.addedToCart;
    map["createdTimestamp"] = this.createdTimestamp;
    return map;
  }
}
