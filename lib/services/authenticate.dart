import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_login_screen/constants.dart';
import 'package:flutter_login_screen/model/product.dart';
import 'package:flutter_login_screen/model/user.dart';

class FireStoreUtils {
  static FirebaseFirestore firestore = FirebaseFirestore.instance;
  Reference storage = FirebaseStorage.instance.ref();

  Future<User> getCurrentUser(String uid) async {
    DocumentSnapshot userDocument =
        await firestore.collection(USERS).doc(uid).get();
    if (userDocument != null && userDocument.exists) {
      return User.fromJson(userDocument.data());
    } else {
      return null;
    }
  }

  static Future<User> updateCurrentUser(User user) async {
    return await firestore
        .collection(USERS)
        .doc(user.userID)
        .set(user.toJson())
        .then((document) {
      return user;
    });
  }

  Future<String> uploadUserImageToFireStorage(File image, String userID) async {
    Reference upload = storage.child("images/$userID.png");
    UploadTask uploadTask = upload.putFile(image);
    var downloadUrl =
        await (await uploadTask.whenComplete(() {})).ref.getDownloadURL();
    return downloadUrl.toString();
  }

  static createProduct(Product product) async {
    final reference = firestore
        .collection(PRODUCTS)
        .doc(auth.FirebaseAuth.instance.currentUser.uid)
        .collection(PRODUCTS);
    product.id = reference.doc().id;
    await reference.doc(product.id).set(product.toJson());
  }

  static updateProduct(Product product) async {
    final reference = firestore
        .collection(PRODUCTS)
        .doc(auth.FirebaseAuth.instance.currentUser.uid)
        .collection(PRODUCTS)
        .doc(product.id);
    await reference.set(product.toJson());
  }

  static deleteProduct(Product product) async {
    final reference = firestore
        .collection(PRODUCTS)
        .doc(auth.FirebaseAuth.instance.currentUser.uid)
        .collection(PRODUCTS)
        .doc(product.id);
    await reference.delete();
  }
  static Stream<QuerySnapshot> getProducts() {
    final reference = firestore
        .collection(PRODUCTS)
        .doc(auth.FirebaseAuth.instance.currentUser.uid)
        .collection(PRODUCTS)
        .orderBy("createdTimestamp");
    return reference.snapshots();
  }

  static Future<List<Product>> getProductsInCart() async {
    final reference = firestore
        .collection(PRODUCTS)
        .doc(auth.FirebaseAuth.instance.currentUser.uid)
        .collection(PRODUCTS)
        .where("added_to_cart", isEqualTo: true);
    final querySnapshot = await reference.get();
    final queryDocSnapshots = querySnapshot.docs;
    final List<Product> productList = [];
    queryDocSnapshots.forEach((element) {
      productList.add(Product.fromJson(element.data()));
    });
    return productList;
  }

  static Future<List<Product>> clearCart() async {
  final products = await getProductsInCart();
  products.forEach((element) {
    element.addedToCart = false;
    updateProduct(element);
  });
  }

}
